﻿using System.Collections.Generic;
using System.Linq;

namespace DALRepository
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Update(T entityToUpdate);
        int CommitChanges();
        void Insert(T entity);
    }
}
