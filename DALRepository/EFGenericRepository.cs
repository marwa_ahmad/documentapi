﻿using DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DALRepository
{
    public class EFGenericRepository<T> : IGenericRepository<T> where T : class
    {
        private DbContext _dbContext;

        public DbContext DBConext
        {
            get
            {
                return _dbContext ?? new DocumentDMSEntities();
            }
        }
        public DbSet<T> DbSet
        {
            get
            {
                return DBConext.Set<T>();
            }
        }
        
        public int CommitChanges()
        {
            return DBConext.SaveChanges();
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public T GetById(object id)
        {
            return DbSet.Find(id);
        }

        public void Insert(T entity)
        {
            if (entity != null) DbSet.Add(entity);
        }

        public void Update(T entityToUpdate)
        {
            if (entityToUpdate != null)
            {
                DbSet.Attach(entityToUpdate);
                DBConext.Entry(entityToUpdate).State = EntityState.Modified;
            }
        }
    }
}
