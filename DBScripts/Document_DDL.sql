
/*DB creation*/
USE master;
GO
	IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'DocumentDMS'))
		CREATE DATABASE DocumentDMS
GO
/*Tables creation*/
USE DocumentDMS
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


/*1. DocumentMetadata*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentMetadata]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[DocumentMetadata](
	[Id] [int] NOT NULL identity,
	[Name] [Nvarchar](150) NULL,
	[UploadDate] [datetime] NOT NULL,
	[PhysicalTypeId] [int] NOT NULL,
	[PhysicalPath] [varchar](150) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Size] [int] NOT NULL,	
		
	CONSTRAINT [PK_DocumentMetadata] PRIMARY KEY([Id]),	
	)
	
	SET ANSI_PADDING OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
