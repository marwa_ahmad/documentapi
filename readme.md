# Document API

WebAPI Document upload and download.

## Use cases
[at Google documents](shorturl.at/uGYZ7)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

1. Please run the SQL script file; "DBScripts/Document_DDL.sql" to create the DB and the Documentmetadata table.
2. Please update the connection string; DocumentAPI/DBConnectionString.config, for the server IP, username and password.

## Running the tests

Three Unit test cases for the WebAPI.

## Architecture

Summarized Architecture

![Summarized Architecture](https://bitbucket.org/marwa_ahmad/documentapi/raw/9c8bae4c877523d7acad55f7cdff3dbfdabce96c/readme_files/DocumentMSArchitecture.PNG)

Document Storage Architecture

![Document Storage Architecture](https://bitbucket.org/marwa_ahmad/documentapi/raw/9c8bae4c877523d7acad55f7cdff3dbfdabce96c/readme_files/DocumentStorageType.PNG)

Document Filter Architecture

![Document Filter](https://bitbucket.org/marwa_ahmad/documentapi/raw/9c8bae4c877523d7acad55f7cdff3dbfdabce96c/readme_files/DocumentFilters.PNG)

## Assumptions

If the request contains the HTTP header "Admin" with value of “1” that means it’s an administrator.
Otherwise if the "Username" header is passed then consider the user is Authenticated.
(I read username header in every request to the webservice for simplicity.)

However, for security reason, the suitable practice is to be based on Token Authentication:

1. You send a request to the API with no credentials. It replies back with with an HTTP forbidden.
2. Your API client gets the forbidden and knows it needs to authenticate. It also knows the URL to authenticate against.
3. It sends a POST to that URL with the login information. The server gets that information — perhaps a username, password — and validates it.
4. If valid, the server returns a response body containing the token.
5. The client gets the token and (here’s the first important difference) stores it. In memory, on disk somewhere.
6. The client then issues the first request again, this time sending the token in an HTTP header.
7. The server gets the token and cryptographically unpacks it, then validates if the token is still valid (age, etc.)


## Deployment

## Built With

* [WebAPI](https://dotnet.microsoft.com/apps/aspnet/apis) - The web framework used
* [Autofac](https://autofac.org) - IoC container
* [Entityframework]  - ORM
* [MSSQL]- DB
* [Moq](https://github.com/Moq/moq4/wiki/Quickstart) - Unit testing Mock

## References

* [MSDN: WebAPI Global error handling](https://docs.microsoft.com/en-us/aspnet/web-api/overview/error-handling/web-api-global-error-handling)
* [MSDN: Unit testing controllers WebAPI](https://docs.microsoft.com/en-us/aspnet/web-api/overview/testing-and-debugging/unit-testing-controllers-in-web-api)
* [MSDN: Unit testing with WebAPI](https://docs.microsoft.com/en-us/aspnet/web-api/overview/testing-and-debugging/unit-testing-with-aspnet-web-api)
* [StackExchange: Avoid big partial class](https://softwareengineering.stackexchange.com/questions/193295/implications-of-a-large-partial-class)
* [Good reply @Stackoverflow: Avoid current request dependancy; use DI instead](https://stackoverflow.com/questions/38170467/httpcontext-current-is-null-when-unit-test)
* [Gunnarpeipman: Nlog Configuration](https://github.com/nlog/NLog/wiki/Configuration-file)
* [C-sharpcorner: Autofac with WebAPI](https://www.c-sharpcorner.com/article/using-autofac-with-web-api/)
* [C-sharpcorner: Upload large files with WEB API](https://www.c-sharpcorner.com/article/upload-large-files-in-web-api-with-a-simple-method/)
* [readme file and appveyor ideas](https://github.com/diaakhateeb/VehicleMonitoringSimulation)
* [Make readme file](https://www.makeareadme.com/)
