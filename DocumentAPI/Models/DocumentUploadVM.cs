﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentAPI.Models
{
    public class DocumentUploadVM
    {
        public DocumentGetVM DocumentMetadata { get; set; }
        public byte[] Content { get; set; }
    }
}