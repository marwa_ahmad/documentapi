﻿$(document).ready(function () {
    submitDocumentPage();
});

function submitDocumentPage() {
    $('#frm_Document').on('submit', function (e) {
        e.preventDefault();        
        var form = $(this);
        $.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: 'api/document',
            headers: { 'Admin': '1' },
            data: form.serialize(),
            success: function (data) {
                alert("Upload done successfully");
                alert(data);
            },
            error: function (data) {
                alert(data)
            }
        });
    });
}
