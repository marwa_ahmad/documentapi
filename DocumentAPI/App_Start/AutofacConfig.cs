﻿using Autofac;
using Autofac.Integration.WebApi;
using DAL;
using DALRepository;
using Logging;
using System.Reflection;
using System.Web.Http;

namespace DocumentAPI.App_Start
{

    /// <summary>
    /// Reference: https://www.c-sharpcorner.com/article/using-autofac-with-web-api/
    /// </summary>
    public class AutofacConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        public static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<DocumentManager>().As<IDocumentManager>();            
            builder.RegisterType<DocumentFileStorage>().As<IDocumentStorage>();
            builder.RegisterType<HeaderService>().As<IHeaderService>();
            builder.RegisterType<FileLogger>().As<ILogger>();

            builder.Register(c => new FileLogger()).As<ILogger>();
            builder.RegisterType<CustomExceptionFilter>().OnActivating(e => e.Instance.SetLogger(new FileLogger()));
            builder.RegisterType<GlobalExceptionHandler>().OnActivating(e => e.Instance.SetLogger(new FileLogger()));            

            Container = builder.Build();//Set the dependency resolver to be Autofac.


            return Container;
        }        
    }
}