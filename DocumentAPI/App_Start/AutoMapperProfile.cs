﻿using AutoMapper;
using DAL;
using Models;

namespace DocumentAPI.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public override string ProfileName
        {
            get { return "AutoMapperProfile"; }
        }

        protected override void Configure()
        {
            CreateMaps();
        }

        private static void CreateMaps()
        {
            Mapper.CreateMap<DocumentMetadata, DocumentMetadataModel>();
            Mapper.CreateMap<DocumentMetadataModel, DocumentMetadata>();
        }
    }
}