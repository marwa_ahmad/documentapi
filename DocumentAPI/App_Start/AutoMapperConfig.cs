﻿using AutoMapper;

namespace DocumentAPI.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(x => x.AddProfile<AutoMapperProfile>());
        }
    }
}