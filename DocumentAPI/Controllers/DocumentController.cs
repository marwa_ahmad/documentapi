﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Models;

namespace DocumentAPI.Controllers
{
    public class DocumentController: ApiController
    {
        private IDocumentManager _documentMgr;
        private IHeaderService _headerService;
        private IDocumentStorage _documentStorage;

        public DocumentController(IDocumentManager documentManager, IHeaderService headerService, IDocumentStorage documentStorage)//will be injected via IoC
        {
            _documentMgr = documentManager;
            _headerService = headerService;
            _documentStorage = documentStorage;
        }

        // GET api/values/5
        /// <summary>
        /// Retrieve a document's metadata by its id
        /// </summary>
        /// <param name="id">document's Id</param>
        /// <returns>HttpResponseMessage which has HttpStatusCode and the data</returns>
        [DocumentAuthorization]
        public HttpResponseMessage Get(int id)
        {
            //get the document's path by id then return the document's meta data and download the document
            HttpResponseMessage response = null;
            var documentMetadata = _documentMgr.GetDocument(id);
            if (documentMetadata == null)
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.OK, documentMetadata);
            }
            return response;
        }

        /// <summary>
        /// Retrieves all documents' metadata
        /// </summary>
        /// <returns>HttpResponseMessage which has HttpStatusCode and the data</returns>
        [DocumentAuthorization]
        public HttpResponseMessage Get()
        {
            //get the document's path by id then return the document's meta data and download the document
            HttpResponseMessage response = null;
            var documentsMetadata = _documentMgr.GetAllDocuments();
            response = Request.CreateResponse(HttpStatusCode.OK, documentsMetadata);
            return response;
        }

        [DocumentAuthorization(Roles = "Admin")]
        /// <summary>
        /// Upload documents
        /// </summary>
        /// <returns>HttpResponseMessage which has HttpStatusCode and the created documents ids</returns>
        // POST api/values
        public HttpResponseMessage Post()
        {            
            var files = _headerService.GetFiles();
            var documentsIds = new List<int>();
            var documentId = -1;

            HttpResponseMessage response = null;
            //TODO upload could be done asynchrounsouly
            if (files != null && files.Count > 0)
            {                
                foreach (string file in files)
                {
                    var postedFile = files[file];
                    var filePath = _documentStorage.SaveDocument(postedFile);
                    documentId = _documentMgr.AddDocumentMetadata(
                                    new DocumentMetadataModel()
                                    {
                                        Name = postedFile.FileName,
                                        PhysicalTypeId = (int)_documentStorage.StorageType,
                                        PhysicalPath = filePath,
                                        Size = postedFile.ContentLength,
                                        UserName = _headerService.GetUserNameFromHeader(), //TODO: pass the session's user id 
                                    });
                    documentsIds.Add(documentId);
                }
                response = Request.CreateResponse(HttpStatusCode.Created, documentsIds);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return response;
        }
    }
}
