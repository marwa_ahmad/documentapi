﻿using Logging;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DocumentAPI
{
    public class LogRequestHandler: DelegatingHandler
    {        
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var logger = (ILogger)request.GetDependencyScope().GetService(typeof(ILogger));
            logger.LogInfo(request.RequestUri.AbsolutePath);
            // let other handlers process the request
            return await base.SendAsync(request, cancellationToken);
        }
    }
}