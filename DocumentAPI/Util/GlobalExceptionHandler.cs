﻿using Logging;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace DocumentAPI
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        private ILogger _logger = new FileLogger();

        public void SetLogger(ILogger logger)
        {
            _logger = logger;
        }
        public async override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            // Access Exception using context.Exception;  
            const string errorMessage = Messages.UNHANDLED_EXCEPTION;
            _logger.LogExption(context.Exception);

            var response = 
                context.Request.CreateResponse(HttpStatusCode.InternalServerError,
                new
                {
                    Message = Messages.UNHANDLED_EXCEPTION
                });
            response.Headers.Add("X-Error", errorMessage);
            context.Result = new ResponseMessageResult(response);
        }
    }
}