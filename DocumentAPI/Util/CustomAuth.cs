﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DocumentAPI
{
    public class DocumentAuthorization : AuthorizeAttribute
    {
        public const string AUTH_HEADER_USERNAME = "Username";
        public const string AUTH_HEADER_ADMIN = "Admin";

        public const string AUTH_HEADER_ADMIN_VALUE = "1";
        public string RequiredRole { get; set; }        
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IsAuthorized(actionContext))
            {
                HandleUnauthorizedRequest(actionContext);
            }
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (((System.Web.HttpContext.Current.User).Identity).IsAuthenticated)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            else
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
        }
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var currentIdentity = actionContext.RequestContext.Principal.Identity;
            var isAuth = false;
            if (CheckHeaderWithValues(actionContext, AUTH_HEADER_USERNAME))
            {
                Roles = "Normal";
                isAuth = true;
            }
            else if (CheckHeaderWithValues(actionContext, AUTH_HEADER_ADMIN, AUTH_HEADER_ADMIN_VALUE))
            {
                Roles = AUTH_HEADER_ADMIN;
                isAuth = true;
            }
            return isAuth;
        }

        /// <summary>
        /// checks if request's headers contains the passed headerName and optional matched with the given value.
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="headerName"></param>
        /// <param name="headerValue">optional matching the request's header's value with the given value</param>
        /// <returns></returns>
        private bool CheckHeaderWithValues(HttpActionContext actionContext, string headerName, string headerValue = null)
        {
            var isAuth = false;
            IEnumerable<string> headerValues;
            if (actionContext.Request.Headers.TryGetValues(headerName, out headerValues))
            {
                var requestHeaderValue = headerValues.FirstOrDefault();
                if ((headerValue == null)
                    || (headerValue != null && string.CompareOrdinal(requestHeaderValue, headerValue) == 0))
                {
                    isAuth = true;
                }
            }
            return isAuth;
        }
    }
}