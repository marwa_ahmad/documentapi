﻿
using System.Collections.Generic;

namespace DocumentAPI
{
    public interface IKeyRepository
    {
        bool HasRole(string key, string role);
        bool Exists(string key);
        void AddKeyRoles(string key, List<string> roles);
        bool RemoveKey(string key);
        List<string> GetRoles(string key);
    }
}
