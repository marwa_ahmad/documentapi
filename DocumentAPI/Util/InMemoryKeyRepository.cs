﻿using System.Collections.Generic;

namespace DocumentAPI
{
    public class InMemoryUserRole
    {
        public string RoleName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
    public class InMemoryKeyRepository : IKeyRepository
    {
        /// <summary>
        /// holds the session/token id and the corresponding roles of that user.
        /// </summary>
        private static Dictionary<string, List<string>> _keys;

        public InMemoryKeyRepository()
        {
            if(_keys == null) _keys = new Dictionary<string, List<string>>();
        }
        public bool Exists(string key)
        {
            return _keys.ContainsKey(key);
        }

        public bool HasRole(string key, string role)
        {
            return Exists(key) && _keys[key].Contains(role);
        }
        public void AddKeyRoles(string key, List<string> roles)
        {
            if (Exists(key)) return;
            _keys.Add(key, roles);
        }

        /// <summary>
        /// used for signing out 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool RemoveKey(string key)
        {
            if (!Exists(key)) return true;
            return _keys.Remove(key);
        }

        public List<string> GetRoles(string key)
        {
            return Exists(key) ? _keys[key] : null;
        }
    }
}