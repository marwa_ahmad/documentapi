﻿using Logging;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace DocumentAPI
{
    /// <summary>
    /// https://www.c-sharpcorner.com/article/exception-handling-in-asp-net-web-api/
    /// </summary>
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        private ILogger _logger = new FileLogger();

        public void SetLogger(ILogger logger)
        {
            _logger = logger;
        }
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //We can log this exception message to the file or database; according to the registed type via the IoC.  
            _logger.LogExption(actionExecutedContext.Exception);
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(Messages.UNHANDLED_EXCEPTION),
                ReasonPhrase = Messages.INTERNAL_SERVER_ERROR,
            };
            actionExecutedContext.Response = response;
        }
    }
}