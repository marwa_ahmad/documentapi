﻿
namespace DocumentAPI
{
    public static class Messages
    {
        public const string INTERNAL_SERVER_ERROR = "Internal Server Error. Please Contact your Administrator.";
        public const string UNHANDLED_EXCEPTION = "An unhandled exception was thrown by service.";
    }
}