﻿using System.Web;

namespace DocumentAPI
{
    public enum DocumentStorageType { FILE = 0, CLOUD = 1, DB = 2 }

    public interface IDocumentStorage
    {
        DocumentStorageType StorageType { get; }
        string SaveDocument(HttpPostedFile file);
    }    
}
