﻿using System.Configuration;
using System.Web;

namespace DocumentAPI
{
    public class DocumentFileStorage : IDocumentStorage
    {
        public DocumentStorageType StorageType { get { return DocumentStorageType.FILE; } }

        public string SaveDocument(HttpPostedFile file)
        {            
            string Path = ConfigurationManager.AppSettings["DocumentsFileUploadLocation"].ToString();
            var filePath = Path + file.FileName;
            file.SaveAs(filePath);
            return filePath;
        }
    }
}