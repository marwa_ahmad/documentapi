﻿using System.Web;

namespace DocumentAPI
{
    public class HeaderService : IHeaderService
    {
        public HttpFileCollection GetFiles()
        {
            var request = HttpContext.Current.Request;
            return request.Files;
        }
        public string GetUserNameFromHeader()
        {
            return HttpContext.Current.Request.Headers["Username"];
        }
    }
}