﻿using System.Web;

namespace DocumentAPI
{
    public interface IHeaderService
    {
        HttpFileCollection GetFiles();
        string GetUserNameFromHeader();
    }
}
