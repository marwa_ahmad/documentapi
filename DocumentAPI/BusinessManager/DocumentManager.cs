﻿using AutoMapper;
using DAL;
using DALRepository;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace DocumentAPI
{

    public class DocumentManager : IDocumentManager
    {        
        private IGenericRepository<DocumentMetadata> _documentRepository;
        public DocumentManager()
        {
            _documentRepository = new EFGenericRepository<DocumentMetadata>();// to be injected via IoC
        }
        public DocumentMetadataModel GetDocument(int id)
        {
            var documentMetadata = _documentRepository.GetById(id);
            return Mapper.Map<DocumentMetadataModel>(documentMetadata);
        }


        public IEnumerable<DocumentMetadataModel> GetAllDocuments()
        {
            var allDocuments = _documentRepository.GetAll();
            var result = Mapper.Map<IEnumerable<DocumentMetadataModel>>(allDocuments);
            return result;
        }

        /// <summary>
        /// Saves the newly created documentMetadat into the database.
        /// </summary>
        /// <param name="documentMetadata"></param>
        /// <returns>the number of updated records</returns>
        public int AddDocumentMetadata(DocumentMetadataModel documentMetadata)//CreateDocumentVM document)
        {
            _documentRepository.Insert(Mapper.Map<DocumentMetadata>(documentMetadata));
            return _documentRepository.CommitChanges();
        }

        /// <summary>
        /// Updates the document's metadata
        /// </summary>
        /// <param name="updatedDocumentMetadata">Entity to be updated</param>
        /// <returns>number of updated records</returns>
        public int UpdateDocumentMetadata(DocumentMetadataModel updatedDocumentMetadata)
        {
            _documentRepository.Update(Mapper.Map<DocumentMetadata>(updatedDocumentMetadata));
            return _documentRepository.CommitChanges();
        }
    }
}
