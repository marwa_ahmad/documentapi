﻿using Models;
using System.Collections.Generic;
using System.Linq;

namespace DocumentAPI
{
    public interface IDocumentManager
    {
        DocumentMetadataModel GetDocument(int id);
        IEnumerable<DocumentMetadataModel> GetAllDocuments();
        int AddDocumentMetadata(DocumentMetadataModel documentMetadata);
        int UpdateDocumentMetadata(DocumentMetadataModel updatedDocumentMetadata);
    }
}
