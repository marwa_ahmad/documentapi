﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentAPI
{
    public static class Extension
    {
        public static bool IsNullOrEmpty<T>(IEnumerable<T> enumrable)
        {
            return enumrable == null || enumrable.Count() == 0;
        }
    }
}