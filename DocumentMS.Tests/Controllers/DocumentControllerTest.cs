﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DocumentAPI.Controllers;
using System.Net.Http;
using Moq;
using DocumentAPI;
using System.Web.Http;

namespace DocumentMS.Tests.Controllers
{
    [TestClass]
    public class DocumentControllerTest
    {
        [TestMethod]
        public void Get()
        {
            // Arrange
            var controller = CreateDocumentController();

            // Act
            HttpResponseMessage result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            var controller = CreateDocumentController();

            controller.Request.Headers.Add("Username", "NormalUser");
            // Act
            HttpResponseMessage result = controller.Get(222);

            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            var controller = CreateDocumentController();            
            controller.Request.Headers.Add("Admin", "1");

            // Act
            var result = controller.Post();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, result.StatusCode);
        }

        private DocumentController CreateDocumentController()
        {
            var mockDocument = new Mock<IDocumentManager>();
            var mockHeaderService = new Mock<IHeaderService>();
            var mockDocumentStorage = new Mock<IDocumentStorage>();

            var controller = new DocumentController(mockDocument.Object, mockHeaderService.Object, mockDocumentStorage.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            return controller;
        }
    }
}
