﻿using System;

namespace Logging
{
    public enum LoggingType {FILE, CLOUD, DB, CONSOLE }

    public interface ILogger
    {
        LoggingType Type { get; }
        void LogInfo(string message);
        void LogExption(Exception e);
    }    
}
