﻿using System;

namespace Logging
{
    /// <summary>
    /// Will be used for testing purpose to log into the console
    /// </summary>
    public class ObjectLogger : ILogger
    {
        public LoggingType Type
        {
            get
            {
                return LoggingType.CONSOLE;
            }
        }

        public void LogExption(Exception e)
        {
            Console.WriteLine(e.Message);
        }

        public void LogInfo(string message)
        {
            Console.WriteLine(message);
        }
    }
}
