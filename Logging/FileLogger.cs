﻿using NLog;
using System;

namespace Logging
{
    public class FileLogger : ILogger
    {
        private Logger Log = LogManager.GetCurrentClassLogger();

        public LoggingType Type
        {
            get
            {
                return LoggingType.FILE;
            }
        }

        public void LogExption(Exception e)
        {
            Log.Log(LogLevel.Error, e.Message, e, null);
        }

        public void LogInfo(string message)
        {
            Log.Log(LogLevel.Info, message);
        }
    }
}
