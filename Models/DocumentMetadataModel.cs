﻿
namespace Models
{
    public class DocumentMetadataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public System.DateTime UploadDate { get; set; }
        public int PhysicalTypeId { get; set; }
        public string PhysicalPath { get; set; }
        public string UserName { get; set; }
        public int Size { get; set; }
    }
}
